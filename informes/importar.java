package informes;

import models.Alumno;
import models.Coneccion;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.String;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class importar{
	ArrayList<Alumno> lista = new ArrayList<>();
	public void setalumnos(){
		JSONParser jsonParser = new JSONParser();
		try (FileReader reader = new FileReader("src/importar.json")){
			Object obj = jsonParser.parse(reader);
			JSONArray alumnosList = (JSONArray) obj;
			alumnosList.forEach( item -> parseObject( (JSONObject) item, lista ) );
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
	}

	private static void parseObject(JSONObject k, ArrayList<Alumno> lista){
		SimpleDateFormat formatter1=new SimpleDateFormat("yyyy-MM-dd");
		JSONObject alumObject = (JSONObject) k;
		try {
			lista.add(new Alumno(
					(String) alumObject.get("rut"),
					(String) alumObject.get("nombre"),
					(String) alumObject.get("ape_pat"),
					(String) alumObject.get("ape_mat"),
					formatter1.parse((String) alumObject.get("fecha_nac")),
					formatter1.parse((String) alumObject.get("fecha_ini")),
					(String) alumObject.get("direccion"),
					Math.toIntExact((Long) alumObject.get("telefono")),
					(String) alumObject.get("email")
			));
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}
	}
	public importar(){
    	setalumnos();
      	for (Alumno item : lista) {
      		String comando_SQL = "Insert into alumno values(?,?,?,?,?,?,?,?,?)";
      		Coneccion c = new Coneccion();
      		try {
      			Connection conn = c.conectar();
      			SimpleDateFormat formatter1=new SimpleDateFormat("yyyy-MM-dd");
      			PreparedStatement s = conn.prepareStatement(comando_SQL);

      			s.setString(1, item.getRut());
				s.setString(2, item.getNombre());
				s.setString(3, item.getApellido_paterno());
				s.setString(4, item.getApellido_materno());
				s.setString(5, formatter1.format(item.getFecha_nacimiento()));
				s.setString(6, formatter1.format(item.getFecha_ingreso()));
				s.setString(7, item.getDomicilio());
				s.setString(8, String.valueOf(item.getTelefono()));
				s.setString(9, item.getEmail());
				s.executeUpdate();
      		} catch (Exception ex) {
      			ex.printStackTrace();
      		}
      	}
	}
}