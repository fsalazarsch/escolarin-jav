package informes;

import models.Coneccion;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.ScrollPane;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.io.IOException;
import java.lang.String;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class notas_por_rut extends JFrame{
	
	private JLabel nombre, ap_pat;
    private JTextField nom, ap1;
    private Container ventana;
    private GridLayout rejilla;
    

    public notas_por_rut(){
    	super("Buscar alumno por rut");
    	rejilla = new GridLayout(2,2,10,10);
    	JButton boton = new JButton("Buscar");

    	nombre = new JLabel("Rut Alumno:  ");
    	nom = new JTextField(15);
    	final JTextArea resultado = new JTextArea(10,15);
    	ScrollPane scrollPane = new ScrollPane();
    	scrollPane.setSize(275,100);
    	scrollPane.add(resultado);

    	ventana = getContentPane();
    	ventana.setLayout(new FlowLayout());
    	ventana.add(nombre);
    	ventana.add(nom);
    	ventana.add(boton, BorderLayout.AFTER_LAST_LINE);
    	ventana.add(scrollPane);

    	boton.addActionListener(e -> {
			String rol = nom.getText();
			boolean rpta = false;
			resultado.setText("");

			Coneccion c = new Coneccion();
			try{
				Connection conn = c.conectar();
				String comando_SQL = "select * from alumno where rut = ?";
				PreparedStatement s = conn.prepareStatement(comando_SQL);
				s.setString(1, rol);

				ResultSet r = s.executeQuery();
				while(r.next()){
					rpta = true;
					resultado.append("Rut alumn:\t\t"+r.getString("rut")+"\nNombre:\t\t"+r.getString("nombre")+" "+r.getString("apellido_pat")+" "
							+r.getString("apellido_mat")+"\nFecha nacimiento:\t"+r.getString("fecha_nacimiento")+
							"\nFecha ingreso:\t\t"+r.getString("fecha_ingreso")+"\nDomicilio:\t\t"+r.getString("domicilio")+"\n");
				}
				if(rpta == false)
					resultado.append("No encontrado\n");

				resultado.append("======================================================\n");
				comando_SQL = "select A2.nombre as nom_asig, A1.* from ponderacion A1, asignatura A2 where A2.id = A1.id_asignatura AND A1.rut_alumno = ?";
				s = conn.prepareStatement(comando_SQL);
				s.setString(1, rol);
				r = s.executeQuery();
				resultado.append("Asignatura\tNota Final\tAsistencia\tSituacion\n");
				while(r.next()){
					resultado.append(r.getString("nom_asig")+"\t"+((r.getFloat("nota1")*r.getShort("ponderacion1")/100)+
							(r.getFloat("nota2")*r.getShort("ponderacion2")/100)+(r.getFloat("nota3")*r.getShort("ponderacion3")/100)+
							(r.getFloat("nota4")*r.getShort("ponderacion4")/100))+"\t"+r.getString("asistencia")+"\t"+r.getString("situacion_acad")+"\n");
				}
				if(rpta == false)
					resultado.append("No encontrado\n");
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
		});

    	setSize(300,300);
    	setResizable(false);
    	setVisible(true);
    }

    public static void main( String args[] ) throws IOException, SQLException, ClassNotFoundException{
    	notas_por_rut m = new notas_por_rut();
    	m.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
    }
}