package informes;

import models.Coneccion;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.ScrollPane;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import java.io.IOException;
import java.lang.String;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class listado_profesores extends JFrame{
	
	private JLabel nombre;
    private Container ventana;
    private GridLayout rejilla;

    public listado_profesores(){
    	super("Listado de Profesores");
    	rejilla = new GridLayout(1,2,10,10);
    	JButton boton = new JButton("Buscar");

    	nombre = new JLabel("Nómina de profesores");

    	final JTextArea resultado = new JTextArea(10,15);
    	ScrollPane scrollPane = new ScrollPane();
    	scrollPane.setSize(275,225);
    	scrollPane.add(resultado);

    	ventana = getContentPane();
    	ventana.setLayout(new FlowLayout());
    	ventana.add(nombre);
    	ventana.add(scrollPane);
    	boolean rpta = false;

		Coneccion c = new Coneccion();
		try {
			Connection conn = c.conectar();
			String comando_SQL = "select A3.nombre, apellido_pat, apellido_mat, A2.nombre as nom_asig from asignatura A2, profesor A3 order by A3.apellido_pat, apellido_mat, A3.nombre ,A2.nombre";
			resultado.append("Nombre mantenedores.profesor\tAsignatura\n");

			PreparedStatement s = conn.prepareStatement(comando_SQL);
			ResultSet r = s.executeQuery();

			while(r.next()){
				rpta = true;
				resultado.append(r.getString("nombre")+" "+r.getString("apellido_pat")+
						" "+r.getString("apellido_mat")+"\t"+r.getString("nom_asig")+"\n");
			}
			if(rpta == false)
				resultado.append("No encontrado");
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		setSize(300,300);
		setResizable(false);
		setVisible(true);
    }

    public static void main( String args[] ) throws IOException, SQLException, ClassNotFoundException{
    	listado_profesores m = new listado_profesores();
    	m.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
    }
}