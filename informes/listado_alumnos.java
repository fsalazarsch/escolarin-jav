package informes;

import models.Coneccion;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.ScrollPane;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import java.io.IOException;
import java.lang.String;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class listado_alumnos extends JFrame{
	private JLabel nombre;
	private Container ventana;
	private GridLayout rejilla;

    public listado_alumnos(){
    	super("Listado de Alumnos");
    	rejilla = new GridLayout(1,2,10,10);
    	JButton boton = new JButton("Buscar");
    	nombre = new JLabel("Nómina de alumnos");

    	final JTextArea resultado = new JTextArea(10,15);
    	ScrollPane scrollPane = new ScrollPane();
    	scrollPane.setSize(275,225);
    	scrollPane.add(resultado);

    	ventana = getContentPane();
    	ventana.setLayout(new FlowLayout());
    	ventana.add(nombre);
    	ventana.add(scrollPane);

    	boolean rpta = false;
    	Coneccion c = new Coneccion();
		try {
			Connection conn = c.conectar();
			String comando_SQL = "select A3.nombre, apellido_pat, A2.nombre as nom_asig, A1.* from ponderacion A1,asignatura A2, alumno A3 where A2.id = A1.id_asignatura and A3.rut = A1.rut_alumno ";
			comando_SQL = comando_SQL.concat(" order by A3.apellido_pat, apellido_mat, A3.nombre ,A2.nombre");
			PreparedStatement s = conn.prepareStatement(comando_SQL);

			resultado.append("Nombre mantenedores.alumno\tAsignatura\tNota Final\tAsistencia\tSituacion\n");
			ResultSet r = s.executeQuery(comando_SQL);

			while(r.next()){
				rpta = true;
				resultado.append(r.getString("nombre")+" "+r.getString("apellido_pat")+"\t\t"+
						r.getString("nom_asig")+"\t"+((r.getFloat("nota1")*r.getShort("ponderacion1")/100)+
						(r.getFloat("nota2")*r.getShort("ponderacion2")/100)+(r.getFloat("nota3")*r.getShort("ponderacion3")/100)+
						(r.getFloat("nota4")*r.getShort("ponderacion4")/100))+"\t"+r.getString("asistencia")+"\t"+r.getString("situacion_acad")+"\n");
			}
			if(rpta == false)
				resultado.append("No encontrado");
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		setSize(300,300);
		setResizable(false);
		setVisible(true);
    }

    public static void main( String args[] ) throws IOException, SQLException, ClassNotFoundException{
    	listado_alumnos m = new listado_alumnos();
    	m.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
    }
}