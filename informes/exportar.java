package informes;

import models.Alumno;
import models.Asignatura;
import models.Coneccion;
import models.Ponderacion;
import models.Profesor;
import org.json.simple.JSONArray;
import java.io.FileWriter;
import java.lang.String;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class exportar{

	public void function_exp(String tabla){

		String comando_SQL = "select * from "+tabla;
		Coneccion c = new Coneccion();
		try {
			Connection conn = c.conectar();
			PreparedStatement s = conn.prepareStatement(comando_SQL);
			ResultSet r = s.executeQuery();

			JSONArray jsonArray = new JSONArray();
			FileWriter file = new FileWriter("src/backup/"+tabla+".json");


			while (r.next()) {
				switch (tabla){
					case "alumno":
						Alumno a = new Alumno(r);
						jsonArray.add(a.getJSONObject());
						break;

					case "profesor":
						Profesor p = new Profesor(r);
						jsonArray.add(p.getJSONObject());
						break;

					case "asignatura":
						Asignatura as = new Asignatura(r);
						jsonArray.add(as.getJSONObject());
						break;

					case "ponderacion":
						Ponderacion pond = new Ponderacion(r);
						jsonArray.add(pond.getJSONObject());
						break;

				}
				//System.out.println(a.toJson());
			}
			file.write(jsonArray.toJSONString());
			file.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

    public exportar()
    {
		function_exp("alumno");
		function_exp("profesor");
		function_exp("asignatura");
		function_exp("ponderacion");
   }
}