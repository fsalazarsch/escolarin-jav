package informes;

import models.Coneccion;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.ScrollPane;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.io.IOException;
import java.lang.String;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class notas_por_nombre extends JFrame{

	private JLabel nombre, ap_pat;
	private JTextField nom, ap1;
	private Container ventana;
    private GridLayout rejilla;

    public notas_por_nombre(){
    	super("Buscar alumno por nombre");
    	rejilla = new GridLayout(2,2,10,10);
    	JButton boton = new JButton("Buscar");

    	nombre = new JLabel("Nombre:  ");
    	nom = new JTextField(15);
    	ap_pat = new JLabel("Apellido paterno:  ");
    	ap1 = new JTextField(15);
    	final JTextArea resultado = new JTextArea(10,15);
    	ScrollPane scrollPane = new ScrollPane();
    	scrollPane.setSize(275,100);
    	scrollPane.add(resultado);

    	ventana = getContentPane();
    	ventana.setLayout(new FlowLayout());
    	ventana.add(nombre);
    	ventana.add(nom);
    	ventana.add(ap_pat);
    	ventana.add(ap1);
    	ventana.add(boton, BorderLayout.AFTER_LAST_LINE);
    	ventana.add(scrollPane);

    	boton.addActionListener(e -> {
			resultado.setText("");
			String n_guard = nom.getText();
			String a_guard = ap1.getText();

			boolean rpta = false;
			Coneccion c = new Coneccion();
			try {
				Connection conn = c.conectar();

				String comando_SQL = "select A3.nombre, apellido_pat, A2.nombre as nom_asig, A1.* from ponderacion A1, asignatura A2,alumno A3 where A2.id = A1.id_asignatura and A3.rut = A1.rut_alumno ";
				if(!n_guard.equals(""))
					comando_SQL = comando_SQL.concat(" and A3.nombre = ?");
				if(!a_guard.equals(""))
					comando_SQL = comando_SQL.concat(" and A3.apellido_pat =?");
				comando_SQL = comando_SQL.concat(" order by A3.apellido_pat, apellido_mat, A3.nombre ,A2.nombre");

				PreparedStatement s = conn.prepareStatement(comando_SQL);
				int i = 1;
				if(!n_guard.equals("")) {
					s.setString(i, n_guard);
					i++;
				}
				if(!a_guard.equals(""))
					s.setString(i, a_guard );
				resultado.append("\nNombre alumno\tAsignatura\tNota Final\tAsistencia\tSituacion\n");
				ResultSet r = s.executeQuery();

				while(r.next())
				{
					rpta = true;
					resultado.append(r.getString("nombre")+" "+r.getString("apellido_pat")+"\t\t"+
							r.getString("nom_asig")+"\t"+((r.getFloat("nota1")*r.getShort("ponderacion1")/100)+
							(r.getFloat("nota2")*r.getShort("ponderacion2")/100)+(r.getFloat("nota3")*r.getShort("ponderacion3")/100)+
							(r.getFloat("nota4")*r.getShort("ponderacion4")/100))+"\t"+r.getString("asistencia")+"\t"+r.getString("situacion_acad")+"\n");
				}
				if(rpta == false) {
					resultado.append("No encontrado");
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		});

    	setSize(300,300);
    	setResizable(false);
    	setVisible(true);
    }

    public static void main( String args[] ) throws IOException, SQLException, ClassNotFoundException{
    	notas_por_nombre m = new notas_por_nombre();
    	m.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
    }
}