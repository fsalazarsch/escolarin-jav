import informes.exportar;
import informes.importar;
import informes.listado_alumnos;
import informes.listado_profesores;
import informes.notas_por_nombre;
import informes.notas_por_rut;

import java.awt.Container;
import java.awt.FlowLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

class Menu_principal extends JFrame {

    public Menu_principal() {
        super("Colegio");

        Container menu = getContentPane();
        menu.setLayout(new FlowLayout());


        JMenuBar menu_p = new JMenuBar();

        JMenu Ayuda = new JMenu("Ayuda");

        JMenu imp = new JMenu("Datos");
        JMenuItem imp_dat = new JMenuItem("Importar Alumnos");
        JMenuItem exp_dat = new JMenuItem("Exportar datos");

        imp.add(imp_dat);
        imp.add(exp_dat);

        JMenuItem cre = new JMenuItem("Creditos");
        Ayuda.add(cre);

        JMenu inf = new JMenu("Módulo Informes");
        JMenuItem notas_alu_nom = new JMenuItem("Notas alumno por nombre");
        JMenuItem notas_alu_rut = new JMenuItem("Notas alumno por rut");
        JMenuItem inf_impr = new JMenuItem("Listado de alumnos");
        JMenuItem list_prof = new JMenuItem("Listado de profesores");


        inf.add(notas_alu_nom);
        inf.add(notas_alu_rut);
        inf.addSeparator();
        inf.add(inf_impr);
        inf.add(list_prof);

        JMenu menumant = new JMenu("Mantención");

        JMenu m_prof = new JMenu("Mantenedor de Profesor");
        JMenu m_alu = new JMenu("Mantenedor de Alumnos");
        JMenu m_asig = new JMenu("Mantenedor de Asignatura");
        JMenu m_asig_alu = new JMenu("Mantendor de Ponderación");

        JMenuItem m_prof1 = new JMenuItem("Insertar Profesor");
        JMenuItem m_prof2 = new JMenuItem("Eliminar Profesor");
        JMenuItem m_prof3 = new JMenuItem("Modificar Profesor");
        JMenuItem m_prof4 = new JMenuItem("Buscar Profesor");

        m_prof.add(m_prof1);
        m_prof.add(m_prof2);
        m_prof.add(m_prof3);
        m_prof.add(m_prof4);

        JMenuItem m_alu1 = new JMenuItem("Insertar Alumno");
        JMenuItem m_alu2 = new JMenuItem("Eliminar Alumno");
        JMenuItem m_alu3 = new JMenuItem("Modificar Alumno");
        JMenuItem m_alu4 = new JMenuItem("Buscar Alumno");

        m_alu.add(m_alu1);
        m_alu.add(m_alu2);
        m_alu.add(m_alu3);
        m_alu.add(m_alu4);

        JMenuItem m_asig1 = new JMenuItem("Insertar Asignatura");
        JMenuItem m_asig2 = new JMenuItem("Eliminar Asignatura");
        JMenuItem m_asig3 = new JMenuItem("Modificar Asignatura");
        JMenuItem m_asig4 = new JMenuItem("Buscar Asignatura");

        m_asig.add(m_asig1);
        m_asig.add(m_asig2);
        m_asig.add(m_asig3);
        m_asig.add(m_asig4);

        JMenuItem m_asig_alu1 = new JMenuItem("Insertar Ponderación");
        JMenuItem m_asig_alu2 = new JMenuItem("Eliminar Ponderación");
        JMenuItem m_asig_alu3 = new JMenuItem("Modificar Ponderación");
        JMenuItem m_asig_alu4 = new JMenuItem("Buscar Ponderación");

        m_asig_alu.add(m_asig_alu1);
        m_asig_alu.add(m_asig_alu2);
        m_asig_alu.add(m_asig_alu3);
        m_asig_alu.add(m_asig_alu4);


        menumant.add(m_prof);
        menumant.add(m_alu);
        menumant.add(m_asig);
        menumant.add(m_asig_alu);


        menu_p.add(menumant);
        menu_p.add(inf);
        menu_p.add(imp);
        menu_p.add(Ayuda);

        setJMenuBar(menu_p);


        Icon fondo = new ImageIcon("untitled1.jpg");
        JLabel label = new JLabel();
        label.setIcon(fondo);
        menu.add(label);


        notas_alu_nom.addActionListener(e -> {
			notas_por_nombre b = new notas_por_nombre();
			b.setVisible(true);
		});

        notas_alu_rut.addActionListener(e -> {
			notas_por_rut b = new notas_por_rut();
			b.setVisible(true);
		});

        inf_impr.addActionListener(e -> {
			listado_alumnos b = new listado_alumnos();
			b.setVisible(true);
		});

        list_prof.addActionListener(e -> {
			listado_profesores b = new listado_profesores();
			b.setVisible(true);
		});

        exp_dat.addActionListener(e -> {
			exportar b = new exportar();
			JOptionPane.showMessageDialog(Menu_principal.this, "exportado\n");

		});

        m_prof1.addActionListener(e -> {
			mantenedores.profesor.insertar b = new mantenedores.profesor.insertar();
			b.setVisible(true);
		});

        m_prof2.addActionListener(e -> {
			mantenedores.profesor.eliminar b = new mantenedores.profesor.eliminar();
			b.setVisible(true);
		});

        m_prof3.addActionListener(e -> {
			mantenedores.profesor.modificar b = new mantenedores.profesor.modificar();
			b.setVisible(true);
		});

        m_prof4.addActionListener(e -> {
			mantenedores.profesor.buscar b = new mantenedores.profesor.buscar();
			b.setVisible(true);
		});

        m_alu1.addActionListener(e -> {
			mantenedores.alumno.insertar b = new mantenedores.alumno.insertar();
			b.setVisible(true);
		});

        m_alu2.addActionListener(e -> {
			mantenedores.alumno.eliminar b = new mantenedores.alumno.eliminar();
			b.setVisible(true);
		});

        m_alu3.addActionListener(e -> {
			mantenedores.alumno.modificar b = new mantenedores.alumno.modificar();
			b.setVisible(true);
		});

        m_alu4.addActionListener(e -> {
			mantenedores.alumno.buscar b = new mantenedores.alumno.buscar();
			b.setVisible(true);
		});

        m_asig1.addActionListener(e -> {
			mantenedores.asignatura.insertar b = new mantenedores.asignatura.insertar();
			b.setVisible(true);
		});

        m_asig2.addActionListener(e -> {
			mantenedores.asignatura.eliminar b = new mantenedores.asignatura.eliminar();
			b.setVisible(true);
		});

        m_asig3.addActionListener(e -> {
			mantenedores.asignatura.modificar b = new mantenedores.asignatura.modificar();
			b.setVisible(true);
		});

        m_asig4.addActionListener(e -> {
			mantenedores.asignatura.buscar b = new mantenedores.asignatura.buscar();
			b.setVisible(true);
		});

        m_asig_alu1.addActionListener(e -> {
			mantenedores.ponderacion.insertar b = new mantenedores.ponderacion.insertar();
			b.setVisible(true);
		});

        m_asig_alu2.addActionListener(e -> {
			mantenedores.ponderacion.eliminar b = new mantenedores.ponderacion.eliminar();
			b.setVisible(true);
		});

        m_asig_alu3.addActionListener(e -> {
			mantenedores.ponderacion.modificar b = new mantenedores.ponderacion.modificar();
			b.setVisible(true);
		});

        m_asig_alu4.addActionListener(e -> {
			mantenedores.ponderacion.buscar b = new mantenedores.ponderacion.buscar();
			b.setVisible(true);
		});

        imp_dat.addActionListener(e -> {
			importar b = new importar();
			JOptionPane.showMessageDialog(Menu_principal.this, "Alumnos Ingresados\n");
		});


        cre.addActionListener(e -> JOptionPane.showMessageDialog(Menu_principal.this, "Hecho por: >fsalazarsch<\nPermitida su copia parcial y/o total\n"));


        setResizable(false);
        setSize(600, 400);
        setVisible(true);

    }

    public static void main(String args[]) throws java.lang.Exception {
        Menu_principal m = new Menu_principal();
        m.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}