create table mantenedores.alumno(
rut varchar2(10) primary key,
nombre varchar2(30) not null,
apellido_pat varchar2(30) not null,
apellido_mat varchar2(30) not null,
Fecha_nacimiento date not null,
Fecha_ingreso date not null,
domicilio varchar2(40) not null,
fono number not null,
email varchar2(30) not null
);

create table mantenedores.asignatura(
id varchar2(10) primary key,
nom_asig varchar2(20),
);

create table mantenedores.alumno(
rut varchar2(10) primary key,
nombre varchar2(30) not null,
apellido_pat varchar2(30) not null,
apellido_mat varchar2(30) not null,
profesion varchar2(30) not null,
grado_academico varchar2(30) not null,
domicilio varchar2(40) not null,
fono number not null,
email varchar2(30) not null,
);

create table asignatura_alumno(
rut_alumno varchar2(10),
rut_profesor varchar2(10),
id_asignatura varchar2(10),

nota1 float check (nota1 >=1),
ponderacion1 number check (ponderacion1 >=1),
nota2 float check (nota2 >=1),
ponderacion2 number check (ponderacion2 >=1),
nota3 float check (nota3 >=1),
ponderacion3 number check (ponderacion3 >=1),
nota4 float check (nota4 >=1),
ponderacion4 number check (ponderacion4 >=1),

asistencia number check (asistencia >=0),
situacion_acad varchar(1),

constraint rel_tabp_alumn foreign key (rut_alumno) references mantenedores.alumno (rut),
constraint rel_tabp_prof foreign key (rut_profesor) references mantenedores.alumno (rut),
constraint rel_tabp_asig foreign key (id_asignatura) references mantenedores.asignatura (id),

);


insert into mantenedores.alumno values('1715337-9','asdf','qwerty','zxcvb','01-01-2000','11-04-2009','fake123',765542974,'asd_qwe@hotmail.com');

insert into mantenedores.asignatura values('Inf-002','programacion II');
insert into mantenedores.asignatura values('Inf-011','taller BDD');

insert into mantenedores.alumno value('111111-1','Juan','P�rez','Sandia','12-11-1981','03-03-2002','Calle Larga 232','22222','jperez@xxxccc.cl');

insert into asignatura_alumno values('1715337-9','111111-1','Inf-002',6.5, 35, 6.1, 20, 6.0, 30, 6.3, 15, 80, 'A');

select A2.nombre, A1.asistencia, situacion_acad
from asignatura_alumno A1, mantenedores.asignatura A2
where A2.id = A1.id_asignatura AND A1.rut_alumno ='1715337-9'


select A2.nombre, A1.rut_alumno, situacion_acad
from asignatura_alumno A1, mantenedores.asignatura A2
where A2.id = A1.id_asignatura

select A3.nombre, A2.nombre, A1.situacion_acad
from asignatura_alumno A1, mantenedores.asignatura A2, mantenedores.alumno A3
where A3.rut = A1.rut_alumno and A2.id = A1.id_asignatura 

3.1)
select A3.nombre, apellido_pat, A2.nombre, A1.situacion_acad
from asignatura_alumno A1, mantenedores.asignatura A2, mantenedores.alumno A3
where A2.id = A1.id_asignatura  and A3.rut = A1.rut_alumno 
order by A3.apellido_pat, apellido_mat, A3.nombre ,A2.nombre

select A3.nombre, apellido_pat, A2.nombre, A1.situacion_acad
from asignatura_alumno A1, mantenedores.asignatura A2, mantenedores.alumno A3
where A2.id = A1.id_asignatura  and A3.rut = A1.rut_alumno and A3.nombre= 'asdf' and A3.apellido_pat = 'qwerty'
order by A3.apellido_pat, apellido_mat, A3.nombre ,A2.nombre