package models;

import org.json.simple.JSONObject;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Ponderacion {
    private String rut_alumno;
    private String rut_profesor;
    private String id_asignatura;
    private float nota1;
    private int ponderacion1;
    private float nota2;
    private int ponderacion2;
    private float nota3;
    private int ponderacion3;
    private float nota4;
    private int ponderacion4;
    private int asistencia;
    private String situacion_acad;

    public Ponderacion(String rut_alumno, String rut_profesor, String id_asignatura,
                       float nota1, float nota2, float nota3, float nota4,
                       int ponderacion1, int ponderacion2, int ponderacion3, int ponderacion4,
                       int asistencia, String situacion_acad) {
        this.rut_alumno = rut_alumno;
        this.rut_profesor = rut_profesor;
        this.id_asignatura = id_asignatura;
        this.nota1 = nota1;
        this.nota2 = nota2;
        this.nota3 = nota3;
        this.nota4 = nota4;
        this.ponderacion1 = ponderacion1;
        this.ponderacion2 = ponderacion2;
        this.ponderacion3 = ponderacion3;
        this.ponderacion4 = ponderacion4;
        this.asistencia = asistencia;
        this.situacion_acad = situacion_acad;
    }


    public Ponderacion(ResultSet r) throws SQLException {
        this.rut_alumno = r.getString("rut_alumno");
        this.rut_profesor = r.getString("rut_profesor");
        this.id_asignatura = r.getString("id_asignatura");
        this.nota1 = Float.parseFloat(r.getString("nota1"));
        this.nota2 = Float.parseFloat(r.getString("nota2"));
        ;
        this.nota3 = Float.parseFloat(r.getString("nota3"));
        ;
        this.nota4 = Float.parseFloat(r.getString("nota3"));
        ;
        this.ponderacion1 = Integer.parseInt(r.getString("ponderacion1"));
        this.ponderacion2 = Integer.parseInt(r.getString("ponderacion2"));
        this.ponderacion3 = Integer.parseInt(r.getString("ponderacion3"));
        this.ponderacion4 = Integer.parseInt(r.getString("ponderacion4"));
        this.asistencia = Integer.parseInt(r.getString("asistencia"));
        this.situacion_acad = r.getString("situacion_acad");
    }


    public String getRut_alumno() {
        return rut_alumno;
    }

    public void setRut_alumno(String rut_alumno) {
        this.rut_alumno = rut_alumno;
    }

    public String getRut_profesor() {
        return rut_profesor;
    }

    public void setRut_profesor(String rut_profesor) {
        this.rut_profesor = rut_profesor;
    }

    public String getId_asignatura() {
        return id_asignatura;
    }

    public void setId_asignatura(String id_asignatura) {
        this.id_asignatura = id_asignatura;
    }

    public float getNota1() {
        return nota1;
    }

    public void setNota1(float nota1) {
        this.nota1 = nota1;
    }

    public int getPonderacion1() {
        return ponderacion1;
    }

    public void setPonderacion1(int ponderacion1) {
        this.ponderacion1 = ponderacion1;
    }

    public float getNota2() {
        return nota2;
    }

    public void setNota2(float nota2) {
        this.nota2 = nota2;
    }

    public int getPonderacion2() {
        return ponderacion2;
    }

    public void setPonderacion2(int ponderacion2) {
        this.ponderacion2 = ponderacion2;
    }

    public float getNota3() {
        return nota3;
    }

    public void setNota3(float nota3) {
        this.nota3 = nota3;
    }

    public int getPonderacion3() {
        return ponderacion3;
    }

    public void setPonderacion3(int ponderacion3) {
        this.ponderacion3 = ponderacion3;
    }

    public float getNota4() {
        return nota4;
    }

    public void setNota4(float nota4) {
        this.nota4 = nota4;
    }

    public int getPonderacion4() {
        return ponderacion4;
    }

    public void setPonderacion4(int ponderacion4) {
        this.ponderacion4 = ponderacion4;
    }

    public int getAsistencia() {
        return asistencia;
    }

    public void setAsistencia(int asistencia) {
        this.asistencia = asistencia;
    }

    public String getSituacion_acad() {
        return situacion_acad;
    }

    public void setSituacion_acad(String situacion_acad) {
        this.situacion_acad = situacion_acad;
    }


    public String toString(String sep) {
        return "rut_alumno='" + rut_alumno + sep +
                "rut_profesor='" + rut_profesor + sep +
                "id_asignatura='" + id_asignatura + sep +
                "nota1=" + nota1 + sep +
                "ponderacion1=" + ponderacion1 + sep +
                "nota2=" + nota2 + sep +
                "ponderacion2=" + ponderacion2 + sep +
                "nota3=" + nota3 + sep +
                "ponderacion3=" + ponderacion3 + sep +
                "nota4=" + nota4 + sep +
                "ponderacion4=" + ponderacion4 + sep +
                "asistencia=" + asistencia + sep +
                "situacion_acad='" + situacion_acad;
    }

    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("rut_alumno", rut_alumno);
            obj.put("rut_profesor", rut_profesor);
            obj.put("id_asignatura", id_asignatura);
            obj.put("nota1", nota1);
            obj.put("nota2", nota2);
            obj.put("nota3", nota3);
            obj.put("nota4", nota4);
            obj.put("ponderacion1", ponderacion1);
            obj.put("ponderacion2", ponderacion2);
            obj.put("ponderacion3", ponderacion3);
            obj.put("ponderacion4", ponderacion4);
            obj.put("asistencia", asistencia);
            obj.put("situacion_acad", situacion_acad);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return obj;
    }
}
