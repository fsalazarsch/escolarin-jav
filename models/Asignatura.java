package models;

import org.json.simple.JSONObject;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Asignatura {
    private String id;
    private String nombre;

    public Asignatura(String id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }


    public Asignatura(ResultSet r) throws SQLException {
        this.id = r.getString("id");
        this.nombre = r.getString("nombre");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String toString(String sep) {
        return "Id: " + id + sep +
                "Nombre: " + nombre;
    }

    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("id", id);
            obj.put("nombre", nombre);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return obj;
    }
}
