package models;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Persona {
    protected String rut;
    protected String nombre;
    protected String apellido_paterno;
    protected String apellido_materno;
    protected String domicilio;
    protected int fono;
    protected String email;


    public Persona(String rut, String nombre, String apellido_paterno, String apellido_materno, String domicilio, int telefono, String email) {
        this.rut = rut;
        this.nombre = nombre;
        this.apellido_paterno = apellido_paterno;
        this.apellido_materno = apellido_materno;
        this.domicilio = domicilio;
        this.fono = telefono;
        this.email = email;
    }

    public Persona(ResultSet r) throws SQLException {
        this.rut = r.getString("rut");
        this.nombre = r.getString("nombre");
        this.apellido_paterno = r.getString("apellido_pat");
        this.apellido_materno = r.getString("apellido_mat");
        this.domicilio = r.getString("domicilio");
        this.fono = r.getInt("fono");
        this.email = r.getString("email");
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido_paterno() {
        return apellido_paterno;
    }

    public void setApellido_paterno(String apellido_paterno) {
        this.apellido_paterno = apellido_paterno;
    }

    public String getApellido_materno() {
        return apellido_materno;
    }

    public void setApellido_materno(String apellido_materno) {
        this.apellido_materno = apellido_materno;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getTelefono() {
        return fono;
    }

    public void setTelefono(int telefono) {
        this.fono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String toString(String sep) {
        return "Rut: " + rut + sep +
                "Nombre: " + nombre + " " + apellido_paterno + " " + apellido_materno + sep +
                "Domicilio: " + domicilio + sep +
                "Fono: " + fono + sep +
                "Email: " + email;
    }

}
