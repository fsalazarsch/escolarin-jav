package models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Coneccion {

    private static Connection c = null;

    public static Connection conectar() throws SQLException, ClassNotFoundException {

        String url = "jdbc:mysql://localhost/";
        String bdd = "colegio";
        String usuario = "root";
        String clave = "";

        if (c == null) {
            try {
                //Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
                Class.forName("com.mysql.cj.jdbc.Driver");
                c = DriverManager.getConnection(url + bdd, usuario, clave);

            } catch (ClassNotFoundException ex) {
                throw new ClassCastException(ex.getMessage());
            } catch (SQLException ex2) {
                throw new SQLException(ex2);
            }

        }
        return c;
    }

    public static void cerrar() throws SQLException {
        if (c != null) {
            c.close();
        }
    }
}

