package models;

import org.json.simple.JSONObject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class Alumno extends Persona {

    private Date fecha_nacimiento;
    private Date fecha_ingreso;

    public Alumno(String rut, String nombre, String apellido_paterno, String apellido_materno, Date fecha_nacimiento, Date fecha_ingreso, String domicilio, int telefono, String email) {
        super(rut, nombre, apellido_paterno, apellido_materno, domicilio, telefono, email);
        this.fecha_nacimiento = fecha_nacimiento;
        this.fecha_ingreso = fecha_ingreso;
    }

    public Alumno(ResultSet r) throws SQLException {
        super(r);
        this.fecha_nacimiento = r.getDate("fecha_nacimiento");
        this.fecha_ingreso = r.getDate("fecha_ingreso");
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido_paterno() {
        return apellido_paterno;
    }

    public void setApellido_paterno(String apellido_paterno) {
        this.apellido_paterno = apellido_paterno;
    }

    public String getApellido_materno() {
        return apellido_materno;
    }

    public void setApellido_materno(String apellido_materno) {
        this.apellido_materno = apellido_materno;
    }

    public Date getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(Date fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public Date getFecha_ingreso() {
        return fecha_ingreso;
    }

    public void setFecha_ingreso(Date fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getTelefono() {
        return fono;
    }

    public void setTelefono(int telefono) {
        this.fono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String toString(String sep) {
        return "Rut: " + rut + sep +
                "Nombre: " + nombre + " " + apellido_paterno + " " + apellido_materno + sep +
                "Fecha Nacimiento: " + fecha_nacimiento + sep +
                "Fecha Ingreso: " + fecha_ingreso + sep +
                "Domicilio: " + domicilio + sep +
                "Fono: " + fono + sep +
                "Email: " + email;
    }

    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("rut", rut);
            obj.put("nombre", nombre);
            obj.put("apellido_paterno", apellido_paterno);
            obj.put("apellido_materno", apellido_materno);
            obj.put("fecha_nacimiento", fecha_nacimiento.toString());
            obj.put("fecha_ingreso", fecha_ingreso.toString());
            obj.put("domicilio", domicilio);
            obj.put("fono", fono);
            obj.put("email", email);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return obj;
    }

}
