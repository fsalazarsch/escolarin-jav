package models;

import org.json.simple.JSONObject;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Profesor {
    private String rut;
    private String nombre;
    private String apellido_paterno;
    private String apellido_materno;
    private String profesion;
    private String grado_academico;
    private String domicilio;
    private int fono;
    private String email;

    public Profesor(String rut, String nombre, String apellido_paterno, String apellido_materno, String profesion, String grado_academico, String domicilio, int telefono, String email) {
        this.rut = rut;
        this.nombre = nombre;
        this.apellido_paterno = apellido_paterno;
        this.apellido_materno = apellido_materno;
        this.profesion = profesion;
        this.grado_academico = grado_academico;
        this.domicilio = domicilio;
        this.fono = telefono;
        this.email = email;
    }

    public Profesor(ResultSet r) throws SQLException {
        this.setRut(r.getString("rut"));
        this.setNombre(r.getString("nombre"));
        this.setApellido_paterno(r.getString("apellido_pat"));
        this.setApellido_materno(r.getString("apellido_mat"));
        this.setProfesion(r.getString("profesion"));
        this.setGrado_academico(r.getString("grado_academico"));
        this.setDomicilio(r.getString("domicilio"));
        this.setTelefono(r.getInt("fono"));
        this.setEmail(r.getString("email"));
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido_paterno() {
        return apellido_paterno;
    }

    public void setApellido_paterno(String apellido_paterno) {
        this.apellido_paterno = apellido_paterno;
    }

    public String getApellido_materno() {
        return apellido_materno;
    }

    public void setApellido_materno(String apellido_materno) {
        this.apellido_materno = apellido_materno;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getTelefono() {
        return fono;
    }

    public void setTelefono(int telefono) {
        this.fono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String toString(String sep) {
        return "Rut: " + rut + sep +
                "Nombre: " + nombre + " " + apellido_paterno + " " + apellido_materno + sep +
                "Grado: " + grado_academico + sep +
                "Profesion: " + profesion + sep +
                "Domicilio: " + domicilio + sep +
                "Fono: " + fono + sep +
                "Email: " + email;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    public String getGrado_academico() {
        return grado_academico;
    }

    public void setGrado_academico(String grado_academico) {
        this.grado_academico = grado_academico;
    }

    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("rut", rut);
            obj.put("nombre", nombre);
            obj.put("apellido_paterno", apellido_paterno);
            obj.put("apellido_materno", apellido_materno);
            obj.put("profesion", profesion);
            obj.put("grado_academico", grado_academico);
            obj.put("domicilio", domicilio);
            obj.put("fono", fono);
            obj.put("email", email);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return obj;
    }
}
