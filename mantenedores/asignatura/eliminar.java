package mantenedores.asignatura;

import models.Coneccion;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.ScrollPane;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.io.IOException;
import java.lang.String;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class eliminar extends JFrame{
	
	private JLabel nombre, ap_pat;
    private JTextField nom, ap1;
    private Container ventana;
    private GridLayout rejilla;
    

    public eliminar(){
    	super("Eliminar Asignatura");
    	
    	rejilla = new GridLayout(2,2,10,10);//filas,columnas,espacio
    	JButton boton = new JButton("Eliminar");

    	nombre = new JLabel("Id. Asignatura:  ");
    	nom = new JTextField(15);
    	final JTextArea resultado = new JTextArea(10,15);
    	ScrollPane scrollPane = new ScrollPane();
    	scrollPane.setSize(275,100);
    	scrollPane.add(resultado);

    	ventana = getContentPane();
    	ventana.setLayout(new FlowLayout());
    	ventana.add(nombre);
    	ventana.add(nom);
    	ventana.add(boton, BorderLayout.AFTER_LAST_LINE);

    	boton.addActionListener(e -> {
			String rol = nom.getText();
			String comando_SQL = "DELETE FROM asignatura WHERE id =?";
			Coneccion c = new Coneccion();
			try {
				Connection conn = c.conectar();
				PreparedStatement s = conn.prepareStatement(comando_SQL);
				s.setString(1, rol);
				s.executeUpdate();
				JOptionPane.showMessageDialog( eliminar.this,"Asignatura eliminada con éxito");
				dispose();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		});

    	setSize(300,300);
    	setResizable(false);
    	setVisible(true);
    }

    public static void main( String args[] ) throws IOException, SQLException, ClassNotFoundException{
    	eliminar m = new eliminar();
    	m.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
    }
}