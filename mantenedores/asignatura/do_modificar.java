package mantenedores.asignatura;

import models.Asignatura;
import models.Coneccion;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.ScrollPane;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.lang.String;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class do_modificar extends JFrame{
	
	private JLabel nombre, iden;
    private JTextField nom, i_d;
    private Container ventana;
    private GridLayout rejilla;
    

    public do_modificar(Asignatura a) {
    	super("Modificar Asignatura");
    	
    	rejilla = new GridLayout(2,2,10,10);//filas,columnas,espacio
    	JButton boton = new JButton("Modificar");

    	nombre = new JLabel("Nuevo Nombre:  ");
    	nom = new JTextField(a.getNombre(), 15);
    	iden = new JLabel("Id. Asignatura:  ");
    	i_d = new JTextField(a.getId(), 15);
    	
    	final JTextArea resultado = new JTextArea(10,15);
    	ScrollPane scrollPane = new ScrollPane();
    	scrollPane.setSize(275,100);
    	scrollPane.add(resultado);

    	ventana = getContentPane();
    	ventana.setLayout(new FlowLayout());
    	ventana.add(nombre);
    	ventana.add(nom);
     	ventana.add(iden);
     	ventana.add(i_d);
     	ventana.add(boton, BorderLayout.AFTER_LAST_LINE);

     	boton.addActionListener(e -> {
			 String n_om = nom.getText();
			 String cod = i_d.getText();
			 String comando_SQL = "Update asignatura SET id = ?, nombre = ? where id = ?";
			 Coneccion c = new Coneccion();
			 try {
				 Connection conn = c.conectar();
				 PreparedStatement s = conn.prepareStatement(comando_SQL);
				 s.setString(1, cod);
				 s.setString(2, n_om);
				 s.setString(3, a.getId());
				 s.executeUpdate();
				 JOptionPane.showMessageDialog(do_modificar.this, "Asignatura modificada con éxito");
				 dispose();
			 } catch (Exception ex) {
				 ex.printStackTrace();
			 }
		 });

     	setSize(300,300);
     	setResizable(false);
     	setVisible(true);
    }
}