package mantenedores.asignatura;

import models.Coneccion;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.lang.String;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class insertar extends JFrame{
	
	final private JLabel id, nombre;
    final private JTextField i_d, n_om;
    private Container ventana;
    private GridLayout rejilla;
    
    public insertar() {
    	super("Ingresar Asignatura");
    	
    	rejilla = new GridLayout(3,2,20,20);//filas,columnas,espacio
    	JButton boton = new JButton("Ingresar");
    	id = new JLabel("Id. de Asignatura:  ");
    	i_d = new JTextField(15);
    	nombre = new JLabel("Nombre:  ");
    	n_om = new JTextField(15);

    	ventana = getContentPane();
    	ventana.setLayout(rejilla);
    	ventana.add(id);
    	ventana.add(i_d);
    	ventana.add(nombre);
    	ventana.add(n_om);
    	ventana.add(boton, BorderLayout.AFTER_LAST_LINE);

    	boton.addActionListener(e -> {
			String etiq0 = i_d.getText();
			String etiq1 = n_om.getText();
			String comando_SQL = "Insert into asignatura values(?,?)";
			Coneccion c = new Coneccion();
			try {
				Connection conn = c.conectar();
				PreparedStatement s = conn.prepareStatement(comando_SQL);
				s.setString(1, etiq0);
				s.setString(2, etiq1);
				s.executeUpdate();

				JOptionPane.showMessageDialog( insertar.this,"Asignatura ingresada con éxito");
				dispose();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		});

    	setSize(300,150);
    	setResizable(true);
    	setVisible(true);
    }

   public static void main( String args[] ){
    	insertar m = new insertar();
    	m.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
    }
}