package mantenedores.ponderacion;

import models.Alumno;
import models.Asignatura;
import models.Coneccion;
import models.Item;
import models.Profesor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.io.IOException;
import java.lang.String;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class eliminar extends JFrame {

    private JLabel nombre, ap_pat;
    private JTextField nom, ap1;
    private Container ventana;
    private GridLayout rejilla;


    public eliminar() {
        super("Eliminar Ponderacion");

        rejilla = new GridLayout(4, 2, 10, 10);//filas,columnas,espacio
        ventana = getContentPane();
        ventana.setLayout(rejilla);

        JButton boton = new JButton("Eliminar");

        JComboBox r_alu = new JComboBox<Alumno>();

        Coneccion c = new Coneccion();

        String comando_SQL = "select * from alumno";
        try {
            Connection conn = c.conectar();
            PreparedStatement s = conn.prepareStatement(comando_SQL);
            ResultSet r = s.executeQuery();

            while (r.next()) {
                Alumno a = new Alumno(r);

                r_alu.addItem(new Item(a.getRut(), a.getNombre() + " " + a.getApellido_paterno() + " " + a.getApellido_materno()));

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        JComboBox r_prof = new JComboBox<Profesor>();

        comando_SQL = "select * from profesor";
        try {
            Connection conn = c.conectar();
            PreparedStatement s = conn.prepareStatement(comando_SQL);
            ResultSet r = s.executeQuery();

            while (r.next()) {
                Profesor a = new Profesor(r);

                r_prof.addItem(new Item(a.getRut(), a.getNombre() + " " + a.getApellido_paterno() + " " + a.getApellido_materno()));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        JComboBox i_asig = new JComboBox<Asignatura>();

        comando_SQL = "select * from asignatura";
        try {
            Connection conn = c.conectar();
            PreparedStatement s = conn.prepareStatement(comando_SQL);
            ResultSet r = s.executeQuery();

            while (r.next()) {
                Asignatura a = new Asignatura(r);

                i_asig.addItem(new Item(a.getId(), a.getNombre()));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        ventana.add(new JLabel("Alumno:  "));
        ventana.add(r_alu);
        ventana.add(new JLabel("Profesor:  "));
        ventana.add(r_prof);
        ventana.add(new JLabel("Asignatura:  "));
        ventana.add(i_asig);

        ventana.add(boton, BorderLayout.AFTER_LAST_LINE);


        boton.addActionListener(e -> {

			Item a = (Item) r_alu.getSelectedItem();
			String etiq0 = a.getId();
			a = (Item) r_prof.getSelectedItem();
			String etiq1 = a.getId();
			a = (Item) i_asig.getSelectedItem();
			String etiq2 = a.getId();


			String comando_SQL1 = "DELETE FROM ponderacion WHERE rut_alumno = ? and rut_profesor = ? and id_asignatura = ?";
			Coneccion c1 = new Coneccion();
			try {
				Connection conn = c1.conectar();
				PreparedStatement s = conn.prepareStatement(comando_SQL1);
				s.setString(1, etiq0);
				s.setString(2, etiq1);
				s.setString(3, etiq2);

				s.executeUpdate();
				JOptionPane.showMessageDialog(eliminar.this, "Ponderacion eliminada con éxito");
				dispose();

			} catch (Exception ex) {
				ex.printStackTrace();
			}

		});

        setSize(300, 300);
        setResizable(false);
        setVisible(true);

    }


    public static void main(String args[]) throws IOException, SQLException, ClassNotFoundException {
        eliminar m = new eliminar();
        m.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}