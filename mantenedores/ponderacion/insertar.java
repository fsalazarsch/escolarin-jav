package mantenedores.ponderacion;

import models.Alumno;
import models.Asignatura;
import models.Coneccion;
import models.Item;
import models.Profesor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.lang.String;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class insertar extends JFrame {

    final private JLabel rut_alumn, rut_prof, id_asig, not1, pon1, not2, pon2, not3, pon3, not4, pon4, asistencia, sit_acad;
    final private JComboBox r_alu, r_prof, i_asig;
    final private JTextField n1, p1, n2, p2, n3, p3, n4, p4, asist, si_ac;
    private Container ventana;
    private GridLayout rejilla;

    public insertar() {
        super("Insertar Ponderacion");

        rejilla = new GridLayout(15, 2, 10, 10);//filas,columnas,espacio
        JButton boton = new JButton("Insertar");

        rut_alumn = new JLabel("Rut Alumno:  ");
        rut_prof = new JLabel("Rut Profesor:  ");
        id_asig = new JLabel("Id. Asignatura:  ");
        not1 = new JLabel("Nota 1:  ");
        pon1 = new JLabel("Ponderación 1:  ");
        not2 = new JLabel("Nota 2:  ");
        pon2 = new JLabel("Ponderación 2:  ");
        not3 = new JLabel("Nota 3:  ");
        pon3 = new JLabel("Ponderación 3:  ");
        not4 = new JLabel("Nota 4:  ");
        pon4 = new JLabel("Ponderación 4:  ");
        asistencia = new JLabel("Asistencia:  ");
        sit_acad = new JLabel("Situación Académica:  ");

        r_alu = new JComboBox<Alumno>();

        Coneccion c = new Coneccion();

        String comando_SQL = "select * from alumno";
        try {
            Connection conn = c.conectar();
            PreparedStatement s = conn.prepareStatement(comando_SQL);
            ResultSet r = s.executeQuery();

            while (r.next()) {
                Alumno a = new Alumno(r);

                r_alu.addItem(new Item(a.getRut(), a.getNombre() + " " + a.getApellido_paterno() + " " + a.getApellido_materno()));

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        r_prof = new JComboBox<Profesor>();

        comando_SQL = "select * from profesor";
        try {
            Connection conn = c.conectar();
            PreparedStatement s = conn.prepareStatement(comando_SQL);
            ResultSet r = s.executeQuery();

            while (r.next()) {
                Profesor a = new Profesor(r);

                r_prof.addItem(new Item(a.getRut(), a.getNombre() + " " + a.getApellido_paterno() + " " + a.getApellido_materno()));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        i_asig = new JComboBox<Asignatura>();

        comando_SQL = "select * from asignatura";
        try {
            Connection conn = c.conectar();
            PreparedStatement s = conn.prepareStatement(comando_SQL);
            ResultSet r = s.executeQuery();

            while (r.next()) {
                Asignatura a = new Asignatura(r);

                i_asig.addItem(new Item(a.getId(), a.getNombre()));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        n1 = new JTextField(15);
        p1 = new JTextField(15);
        n2 = new JTextField(15);
        p2 = new JTextField(15);
        n3 = new JTextField(15);
        p3 = new JTextField(15);
        n4 = new JTextField(15);
        p4 = new JTextField(15);
        asist = new JTextField(15);
        si_ac = new JTextField(15);

        ventana = getContentPane();
        ventana.setLayout(rejilla);

        ventana.add(rut_alumn);
        ventana.add(r_alu);
        ventana.add(rut_prof);
        ventana.add(r_prof);
        ventana.add(id_asig);
        ventana.add(i_asig);
        ventana.add(not1);
        ventana.add(n1);
        ventana.add(pon1);
        ventana.add(p1);
        ventana.add(not2);
        ventana.add(n2);
        ventana.add(pon2);
        ventana.add(p2);
        ventana.add(not3);
        ventana.add(n3);
        ventana.add(pon3);
        ventana.add(p3);
        ventana.add(not4);
        ventana.add(n4);
        ventana.add(pon4);
        ventana.add(p4);
        ventana.add(asistencia);
        ventana.add(asist);
        ventana.add(sit_acad);
        ventana.add(si_ac);


        ventana.add(boton, BorderLayout.AFTER_LAST_LINE);

        boton.addActionListener(e -> {

			Item a = (Item) r_alu.getSelectedItem();
			String etiq0 = a.getId();

			a = (Item) r_prof.getSelectedItem();
			String etiq1 = a.getId();

			a = (Item) i_asig.getSelectedItem();
			String etiq2 = a.getId();


			float etiq3 = Float.parseFloat(n1.getText()), etiq5 = Float.parseFloat(n2.getText()), etiq7 = Float.parseFloat(n3.getText()), etiq9 = Float.parseFloat(n4.getText());
			short etiq11 = Short.parseShort(asist.getText());
			String etiq12 = si_ac.getText();
			Short etiq4, etiq6, etiq8, etiq10;

			do {
				etiq4 = Short.parseShort(p1.getText());
				etiq6 = Short.parseShort(p2.getText());
				etiq8 = Short.parseShort(p3.getText());
				etiq10 = Short.parseShort(p4.getText());
			} while ((Short.parseShort(p1.getText()) + Short.parseShort(p2.getText()) + Short.parseShort(p3.getText()) + Short.parseShort(p4.getText())) != 100);

			String comando_SQL1 = "Insert into ponderacion values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
			Coneccion c1 = new Coneccion();
			try {
				Connection conn = c1.conectar();
				PreparedStatement s = conn.prepareStatement(comando_SQL1);
				s.setString(1, etiq0);
				s.setString(2, etiq1);
				s.setString(3, etiq2);
				s.setString(4, String.valueOf(etiq3));
				s.setString(5, String.valueOf(etiq4));
				s.setString(6, String.valueOf(etiq5));
				s.setString(7, String.valueOf(etiq6));
				s.setString(8, String.valueOf(etiq7));
				s.setString(9, String.valueOf(etiq8));
				s.setString(10, String.valueOf(etiq9));
				s.setString(11, String.valueOf(etiq10));
				s.setString(12, String.valueOf(etiq11));
				s.setString(13, etiq12);
				s.executeUpdate();

				JOptionPane.showMessageDialog(insertar.this, "Ponderacion ingresada con éxito");
				dispose();

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		});

        setSize(300, 450);
        setResizable(true);
        setVisible(true);

    }

    public static void main(String args[]) {
        insertar m = new insertar();
        m.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}