package mantenedores.profesor;

import models.Coneccion;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.lang.String;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class insertar extends JFrame {

    final private JLabel rut, nombre, ap_pat, ap_mat, profesion, grado, domicilio, fono, email;
    final private JTextField r_ut, n_om, a_ppat, a_pmat, profesion_val, grado_val, d_om, f_ono, e_mail;
    private Container ventana;
    private GridLayout rejilla;

    public insertar() {
        super("Ingresar Profesor");

        rejilla = new GridLayout(10, 2, 10, 10);//filas,columnas,espacio
        JButton boton = new JButton("Ingresar");
        rut = new JLabel("Rut:  ");
        r_ut = new JTextField(15);
        nombre = new JLabel("Nombre:  ");
        n_om = new JTextField(15);
        ap_pat = new JLabel("Apellido Paterno:  ");
        a_ppat = new JTextField(15);
        ap_mat = new JLabel("Apellido Materno:  ");
        a_pmat = new JTextField(15);
        profesion = new JLabel("Porofesion:  ");
        d_om = new JTextField(15);
        f_ono = new JTextField(15);
        profesion_val = new JTextField(15);
        e_mail = new JTextField(15);
        grado_val = new JTextField(15);

        grado = new JLabel("Grado:  ");
        domicilio = new JLabel("Domicilio:  ");
        fono = new JLabel("Teléfono:  ");
        email = new JLabel("E-Mail:  ");

        ventana = getContentPane();
        ventana.setLayout(rejilla);
        ventana.add(rut);
        ventana.add(r_ut);
        ventana.add(nombre);
        ventana.add(n_om);
        ventana.add(ap_pat);
        ventana.add(a_ppat);
        ventana.add(ap_mat);
        ventana.add(a_pmat);
        ventana.add(profesion);
        ventana.add(profesion_val);
        ventana.add(grado);
        ventana.add(grado_val);
        ventana.add(domicilio);
        ventana.add(d_om);
        ventana.add(fono);
        ventana.add(f_ono);
        ventana.add(email);
        ventana.add(e_mail);

        ventana.add(boton, BorderLayout.AFTER_LAST_LINE);


        boton.addActionListener(e -> {

			String etiq0 = r_ut.getText();
			String etiq1 = n_om.getText();
			String etiq2 = a_ppat.getText();
			String etiq3 = a_pmat.getText();
			String etiq4 = profesion_val.getText();
			String etiq5 = grado_val.getText();
			String etiq6 = d_om.getText();
			String etiq7 = f_ono.getText();
			String etiq8 = e_mail.getText();


			String comando_SQL = "Insert into profesor values(?,?,?,?,?,?,?,?,?)";
			Coneccion c = new Coneccion();
			try {
				Connection conn = c.conectar();
				PreparedStatement s = conn.prepareStatement(comando_SQL);
				s.setString(1, etiq0);
				s.setString(2, etiq1);
				s.setString(3, etiq2);
				s.setString(4, etiq3);
				s.setString(5, etiq4);
				s.setString(6, etiq5);
				s.setString(7, etiq6);
				s.setString(8, etiq7);
				s.setString(9, etiq8);

				s.executeUpdate();
				JOptionPane.showMessageDialog(insertar.this, "Profesor ingresado con éxito");
				dispose();

			} catch (Exception ex) {
				ex.printStackTrace();
			}


		});

        setSize(300, 350);
        setResizable(true);
        setVisible(true);

    }

    public static void main(String args[]) {
        insertar m = new insertar();
        m.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}