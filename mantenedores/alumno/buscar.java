package mantenedores.alumno;

import models.Alumno;
import models.Coneccion;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.ScrollPane;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.io.IOException;
import java.lang.String;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class buscar extends JFrame{
	
	private JLabel nombre, ap_pat;
    private JTextField nom, ap1;
    private Container ventana;
    private GridLayout rejilla;

    public Coneccion c = new Coneccion();

    public buscar(){
    	super("Buscar alumno");
    	rejilla = new GridLayout(2,2,10,10);
    	JButton boton = new JButton("Buscar");
    	nombre = new JLabel("Rut Alumno:  ");
    	nom = new JTextField(15);
    	final JTextArea resultado = new JTextArea(10,10);
    	ScrollPane scrollPane = new ScrollPane();
    	scrollPane.setSize(275,100);
    	scrollPane.add(resultado);

    	ventana = getContentPane();
    	ventana.setLayout(new FlowLayout());
    	ventana.add(nombre);
    	ventana.add(nom);
    	ventana.add(boton, BorderLayout.AFTER_LAST_LINE);
    	ventana.add(scrollPane);

    	boton.addActionListener(e -> {
			String rol = nom.getText();
			boolean rpta = false;
			resultado.setText("");
			String comando_SQL = "select * from alumno where rut = ?";

			Coneccion c = new Coneccion();
			try {
				Connection conn = c.conectar();
				PreparedStatement s = conn.prepareStatement(comando_SQL);
				s.setString(1, rol);
				ResultSet r = s.executeQuery();

				while (r.next()) {
					rpta = true;
					Alumno a = new Alumno(r);
					resultado.append(a.toString("\n"));
					resultado.append("\n======================\n");
				}
				if (rpta == false)
					resultado.append("(No encontrado)\n");
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		});

    	setSize(300,300);
    	setResizable(false);
    	setVisible(true);
    }

    public static void main( String args[] ) throws IOException, SQLException, ClassNotFoundException{
    	buscar m = new buscar();
    	m.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
    }
}