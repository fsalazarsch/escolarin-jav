package mantenedores.alumno;

import models.Alumno;
import models.Coneccion;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.lang.String;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class do_modificar extends JFrame
   {
	
	final private JLabel rut, nombre, ap_pat, ap_mat, f_nac, f_ini, domicilio, fono, email;
    final private JTextField rut_val, nom_val, ap_pat_val, ap_mat_val, f_nac_val, f_ini_val, domicilio_val, fono_val, email_val;
    private Container ventana;
    private GridLayout rejilla;
    

    public do_modificar(Alumno a){ 
    	super("Modificar Alumno");
    	
    	rejilla = new GridLayout(10,2,10,10);//filas,columnas,espacio
    	JButton boton = new JButton("Modificar");

    	rut = new JLabel("Rut:  ");
    	rut_val = new JTextField(a.getRut(), 15);
    	nombre = new JLabel("Nombre:  ");
    	nom_val = new JTextField(a.getNombre(), 15);
    	ap_pat = new JLabel("Apellido Paterno:  ");
    	ap_pat_val = new JTextField(a.getApellido_paterno(), 15);
    	ap_mat = new JLabel("Apellido Materno:  ");
    	ap_mat_val = new JTextField(a.getApellido_materno(), 15);
    	f_nac = new JLabel("Fecha nacimiento:  ");
    	f_nac_val = new JTextField(a.getFecha_nacimiento().toString(), 15);
    	f_ini = new JLabel("Fecha Ingreso:  ");
    	f_ini_val = new JTextField(a.getFecha_ingreso().toString(), 15);
    	domicilio_val = new JTextField(a.getDomicilio(), 15);
    	fono_val = new JTextField(String.valueOf(a.getTelefono()), 15);
    	email_val = new JTextField(a.getEmail(), 15);
    	domicilio = new JLabel("Domicilio:  ");
    	fono = new JLabel("Teléfono:  ");
    	email = new JLabel("E-Mail:  ");

    	ventana = getContentPane();
    	ventana.setLayout(rejilla);
    	ventana.add(rut);
    	ventana.add(rut_val);
    	ventana.add(nombre);
    	ventana.add(nom_val);
    	ventana.add(ap_pat);
    	ventana.add(ap_pat_val);
    	ventana.add(ap_mat);
    	ventana.add(ap_mat_val);
    	ventana.add(f_nac);
    	ventana.add(f_nac_val);
    	ventana.add(f_ini);
    	ventana.add(f_ini_val);
    	ventana.add(domicilio);
    	ventana.add(domicilio_val);
    	ventana.add(fono);
    	ventana.add(fono_val);
    	ventana.add(email);
    	ventana.add(email_val);
    	ventana.add(boton, BorderLayout.AFTER_LAST_LINE);

		boton.addActionListener(e -> {
			String etiq1 = rut_val.getText();
			String etiq2 = nom_val.getText();
			String etiq3 = ap_pat_val.getText();
			String etiq4 = ap_mat_val.getText();
			String etiq5 = f_nac_val.getText();
			String etiq6 = f_ini_val.getText();
			String etiq7 = domicilio_val.getText();
			String etiq8 = fono_val.getText();
			String etiq9 = email_val.getText();

			String comando_SQL = "Update alumno SET rut = ?, nombre = ?, apellido_pat = ?, apellido_mat = ?," +
					"fecha_nacimiento = ?, fecha_ingreso = ?, domicilio = ?, fono = ?, email = ? where rut = ?";
			Coneccion c = new Coneccion();
			try {
				Connection conn = c.conectar();
				PreparedStatement s = conn.prepareStatement(comando_SQL);
				s.setString(1, etiq1);
				s.setString(2, etiq2);
				s.setString(3, etiq3);
				s.setString(4, etiq4);
				s.setString(5, etiq5);
				s.setString(6, etiq6);
				s.setString(7, etiq7);
				s.setString(8, etiq8);
				s.setString(9, etiq9);
				s.setString(10, a.getRut());
				s.executeUpdate();

				JOptionPane.showMessageDialog(do_modificar.this, "Alumno modificado con éxito");
				dispose();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		});

		setSize(300,350);
		setResizable(false);
		setVisible(true);
    }
}