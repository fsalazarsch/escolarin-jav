package mantenedores.alumno;

import models.Alumno;
import models.Coneccion;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.ScrollPane;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class modificar extends JFrame {

	private JLabel nombre, ap_pat;
    private JTextField nom, ap1;
    private Container ventana;
    private GridLayout rejilla;


    public modificar(){
    	super("Modificar alumno");
    	
    	rejilla = new GridLayout(2,2,10,10);//filas,columnas,espacio
    	JButton boton = new JButton("Modificar");

    	nombre = new JLabel("Rut Alumno:  ");
    	nom = new JTextField(15);
    	final JTextArea resultado = new JTextArea(10,15);
    	ScrollPane scrollPane = new ScrollPane();
    	scrollPane.setSize(275,100);
    	scrollPane.add(resultado);

    	ventana = getContentPane();
    	ventana.setLayout(new FlowLayout());
    	ventana.add(nombre);
    	ventana.add(nom);
    	ventana.add(boton, BorderLayout.AFTER_LAST_LINE);
		
    	boton.addActionListener(e -> {
			String rol = nom.getText();
			String comando_SQL = "SELECT * FROM alumno WHERE rut =?";
			Coneccion c = new Coneccion();
			try {
				Connection conn = c.conectar();
				PreparedStatement s = conn.prepareStatement(comando_SQL);
				s.setString(1, rol);
				ResultSet r = s.executeQuery();

				while (r.next()) {
					Alumno a = new Alumno(r);
					do_modificar dm =  new do_modificar(a);
					dispose();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		});

    	setSize(300,300);
    	setResizable(false);
    	setVisible(true);
    }

   public static void main( String args[] ) throws IOException, SQLException, ClassNotFoundException {
    	modificar m = new modificar();
    	m.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
    }
}