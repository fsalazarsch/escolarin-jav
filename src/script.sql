-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table colegio.alumno
DROP TABLE IF EXISTS `alumno`;
CREATE TABLE IF NOT EXISTS `alumno` (
  `rut` varchar(10) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellido_pat` varchar(30) NOT NULL,
  `apellido_mat` varchar(30) NOT NULL,
  `Fecha_nacimiento` date NOT NULL,
  `Fecha_ingreso` date NOT NULL,
  `domicilio` varchar(40) NOT NULL,
  `fono` int(11) NOT NULL,
  `email` varchar(30) NOT NULL,
  PRIMARY KEY (`rut`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table colegio.asignatura
DROP TABLE IF EXISTS `asignatura`;
CREATE TABLE IF NOT EXISTS `asignatura` (
  `id` varchar(10) NOT NULL,
  `nombre` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table colegio.ponderacion
DROP TABLE IF EXISTS `ponderacion`;
CREATE TABLE IF NOT EXISTS `ponderacion` (
  `rut_alumno` varchar(10) DEFAULT NULL,
  `rut_profesor` varchar(10) DEFAULT NULL,
  `id_asignatura` varchar(10) DEFAULT NULL,
  `nota1` float DEFAULT NULL,
  `ponderacion1` int(11) DEFAULT NULL,
  `nota2` float DEFAULT NULL,
  `ponderacion2` int(11) DEFAULT NULL,
  `nota3` float DEFAULT NULL,
  `ponderacion3` int(11) DEFAULT NULL,
  `nota4` float DEFAULT NULL,
  `ponderacion4` int(11) DEFAULT NULL,
  `asistencia` int(11) DEFAULT NULL,
  `situacion_acad` varchar(1) DEFAULT NULL,
  KEY `rel_tabp_alumn` (`rut_alumno`),
  KEY `rel_tabp_prof` (`rut_profesor`),
  KEY `rel_tabp_asig` (`id_asignatura`),
  CONSTRAINT `rel_tabp_alumn` FOREIGN KEY (`rut_alumno`) REFERENCES `alumno` (`rut`),
  CONSTRAINT `rel_tabp_asig` FOREIGN KEY (`id_asignatura`) REFERENCES `asignatura` (`id`),
  CONSTRAINT `rel_tabp_prof` FOREIGN KEY (`rut_profesor`) REFERENCES `profesor` (`rut`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table colegio.profesor
DROP TABLE IF EXISTS `profesor`;
CREATE TABLE IF NOT EXISTS `profesor` (
  `rut` varchar(10) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellido_pat` varchar(30) NOT NULL,
  `apellido_mat` varchar(30) NOT NULL,
  `profesion` varchar(30) NOT NULL,
  `grado_academico` varchar(30) NOT NULL,
  `domicilio` varchar(40) NOT NULL,
  `fono` int(11) NOT NULL,
  `email` varchar(30) NOT NULL,
  PRIMARY KEY (`rut`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
